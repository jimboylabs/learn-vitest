import { Todo } from "../../entities/todo";
import { TodoRepository } from "../todo-repository";

export class InMemoryTodoRepository implements TodoRepository {
  public todos: Todo[] = [];

  create(todo: Todo): Promise<void> {
    this.todos.push(todo);
    return Promise.resolve();
  }
};
