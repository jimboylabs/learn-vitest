interface TodoProps {
  title: string;
  done: boolean;
};

export class Todo {
  private props: TodoProps;

  constructor(props: TodoProps) {
    this.props = props;
  }

  get title() {
    return this.props.title;
  }

  get done() {
    return this.props.done;
  }
};
