import { Todo } from "../entities/todo";
import { TodoRepository } from "../repositories/todo-repository";

interface CreateTodoRequest {
  title: string;
};

type CreateTodoResponse = Todo;

export class CreateTodo {
  constructor(private todoRepository: TodoRepository) {
  }

  async execute({ title }: CreateTodoRequest): Promise<CreateTodoResponse> {
    const todo = new Todo({
      title,
      done: false,
    });

    await this.todoRepository.create(todo);

    return todo;
  }
}
