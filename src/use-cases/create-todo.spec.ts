import { describe, expect, it } from "vitest";
import { Todo } from "../entities/todo";
import { InMemoryTodoRepository } from "../repositories/in-memory/in-memory-todo-repository";
import { CreateTodo } from "./create-todo";

describe("Create Todo", () => {
  it("should be able to create a todo item", () => {
    const todoRepository = new InMemoryTodoRepository();
    const createTodo = new CreateTodo(todoRepository);

    expect(createTodo.execute({ title: "Todo Item" })).resolves.toBeInstanceOf(Todo);
  });
});
