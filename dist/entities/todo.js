"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Todo = void 0;
;
class Todo {
    constructor(props) {
        this.props = props;
    }
    get title() {
        return this.props.title;
    }
    get done() {
        return this.props.done;
    }
}
exports.Todo = Todo;
;
